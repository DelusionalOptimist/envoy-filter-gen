module github.com/DelusionalOptimist/stonks

go 1.16

require (
	github.com/envoyproxy/go-control-plane v0.9.9-0.20201210154907-fd9021fe5dad
	github.com/ghodss/yaml v1.0.0
	github.com/gogo/protobuf v1.3.2
	github.com/golang/protobuf v1.5.2
	istio.io/api v0.0.0-20210416170358-17514f58eeeb
)
