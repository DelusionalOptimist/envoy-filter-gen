package main

import (
	"fmt"
	"io/ioutil"

	core "github.com/envoyproxy/go-control-plane/envoy/config/core/v3" // golang protobuf. protoreflect implemented
	yaml "github.com/ghodss/yaml"
	networking "istio.io/api/networking/v1alpha3" // gogo protobuf
)

func main() {
	string := modify("imagehub-filter.yaml")
	fmt.Println(string)
}

func modify(filePath string) string {

	// reading the config
	fileString, err := ioutil.ReadFile(filePath)
	if err != nil {
		return err.Error()
	}

	// convert it to JSON
	some, err := yaml.YAMLToJSON(fileString)
	if err != nil {
		panic(err)
	}

	// Unmarshal it to struct EnvoyFilter
	var m networking.EnvoyFilter
	err = m.UnmarshalJSON(some)
	if err != nil {
		panic(err)
	}

	// get the configPatches
	// TODO: Handle multiple configPatches
	configPatches := m.GetConfigPatches()

	// get value from the configpatch
	configPatchValue := configPatches[0].Patch.GetValue()
	if err != nil {
		panic(err)
	}
	// printing just for fun
	fmt.Println(configPatchValue)

// ------------------------ROADBLOCK-------------------------------------------
// configPatchValue is of type *types.Struct in gogo protobufs implementation.
// We need to Marshal it to TypedExtensionConfig which is implemented by
// envoyproxy/go-control-plane.
// The problem is go-control-plane uses golang.org protobufs and I'm clueless
// on how to do this conversion.

	_ = &core.TypedExtensionConfig{}

	return ("works")
}
